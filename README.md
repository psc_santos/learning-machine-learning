# Learning Machine Learning

Machine Learning assignments from a MOOC called "Applied Machine Learning in Python". This repository is intended for friends and colleagues learning concepts in ML.
It is *not* intended for students of the above-mentioned course.
